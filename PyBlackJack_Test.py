#!/usr/bin/env python3
# Ein Test für das Modul PyBlackJack
# Erstellt: 27/10/20

import unittest
from PyBlackJack import *

class BlackJackTest(unittest.TestCase):

    def testGame(self):
        l = Logger()
        g = Game(l)
        self.assertIsNotNone(g)

    def testGameCards(self):
        l = Logger()
        g = Game(l)
        self.assertTrue(len(g.cardList) == 52)

    def testPlayer(self):
        l = Logger()
        g = Game(l)
        deck = Deck(g.cardList)
        d = Dealer(g, deck, "Donald")
        p = Player(g, d, "Pemo")
        self.assertIsNotNone(p)


unittest.main()