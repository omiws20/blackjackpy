#!/usr/bin/env python3
# Definiert die Klassen für das PyBlackJack-Spiel
# Erstellt: 26/10/20

from datetime import datetime
from os import path, listdir
import random

from enum import Enum

class Suit(Enum):
    Heart = 0
    Diamond = 1
    Spade = 2
    Club = 3

# Definiert den (obligatorischen) Logger
class Logger:

    def __init__(self):
        self.logPath = path.join(path.dirname(__file__), "pyblackjackV2.log")
    
    def logInfo(self, msgText):
        with open(self.logPath, mode="a", encoding="utf8") as fh:
            timeStamp = datetime.today().strftime("%H:%M")
            fh.write(f"{timeStamp}: *** {msgText}\n")

    def logError(self, msgText):
        with open(self.logPath, mode="a", encoding="utf8") as fh:
            timeStamp = datetime.today().strftime("%H:%M")
            fh.write(f"{timeStamp}: !!! {msgText}\n")

# Definiert eine Karte
class Card:

    dicCards = {"One":1,"Two":2,"Three":3,"Four":4,"Five":5,"Six":6,"Seven":7,"Eight":8,"Nine":9,"Ten":10,
         "Jack":10,"Queen":10,"King":10,"Ace":10}

    def __init__(self, Index, Path, Name):
        self.index = Index
        self.imgPath = Path
        self.name = Name
        self.value = Card.dicCards[Name.split("_")[0]]
        self.valueName = Name.split("_")[0]
        self.suit = Suit[Name.split("_")[1]]

# Definiert ein Kartendeck mit 52 Karten
class Deck:
    
    def __init__(self, Cardlist):
        self.cards = Cardlist

# Definiert den Dealer des Spiels
class Dealer:

    def __init__(self, Game, Deck, Name):
        self.game = Game
        self.id = 1
        self.name = Name
        self.deck = Deck
        self.cardHand = []
        self.logger = Game.logger

    def getValue(self):
        value = self.game.Calculate(self.cardHand)
        return value

    def newCard(self, DealerCard=True):
        exitFlag = False
        while not exitFlag:
            z = random.randint(0, len(self.deck.cards) - 1)
            if z not in [c.index for c in self.cardHand]:
                newCard = self.deck.cards[z]
                # Ist die Karte für den Dealer?
                if DealerCard:
                    self.cardHand.append(newCard)
                exitFlag = True
        name = "Dealer" if DealerCard else "Player"
        logMsg = f"*** Calling newCard() for {name}"
        self.logger.logInfo(logMsg)
        return newCard

# Definiert einen Spieler
class Player:

    def __init__(self, Game, Dealer, Name):
        self.game = Game
        self.dealer = Dealer
        self.id = 2
        self.name = Name
        self.cardHand = []
        self.logger = Game.logger
    
    # Dealer gibt eine Karte
    def hit(self):
        newCard = self.dealer.newCard(False)
        self.cardHand.append(newCard)
        logMsg = "*** Calling hit() for Player"
        self.logger.logInfo(logMsg)
        return newCard

    def getValue(self):
        value = self.game.Calculate(self.cardHand)
        return value

# Definiert ein Spiel
class Game:

    # Statische Variablen
    cardsPath = path.join(path.dirname(__file__), "Cards")
    defaultCardname = "green_back.png"
    defaultCardpath = path.join(cardsPath, defaultCardname)

    # Liest alle Bitmapdateien aus einem Verzeichnis ein und gibt eine Liste mit Card-Objekten zurueck
    def ReadCards(self):
        imgPaths = [path.join(Game.cardsPath, f) for f in listdir(Game.cardsPath) if path.splitext(f)[1] == ".png" and f != Game.defaultCardname]
        cardList = []
        for i, imgPath in enumerate(imgPaths):
            cardName = path.splitext(path.basename(imgPath))[0]
            cardList.append(Card(i, imgPath,cardName))
        return cardList

   # Berechnet den Wert eines Kartenstapels
    def Calculate(self, cards):
        value = 0
        for card in cards:
            value += Card.dicCards[card.valueName]
        # Muessen die Asse anders gezaehlt werden?
        if value > 21:
            asse = [c for c in cards if c.valueName == "Ace"]
            for _ in asse:
                # Pro Ass 9 Punkte abziehen, aus Ass+Koenig+9 = 29 wird so 20
                value -= 9
        return value

    def __init__(self, Logger):
        # Alle Listen an einer Stelle initialisieren
        self.cardList = self.ReadCards()
        self.dealerCards = []
        self.playerCards = []
        self.currentDeck = []
        self.startTime = datetime.today
        self.logger = Logger

    def Start(self):
        self.dealerCards = []
        self.playerCards = []
        self.currentDeck = []
        logMsg = "*** Game started"
        self.logger.logInfo(logMsg)
