# Zeigt eine Menueauswahl an
def showMenu(menuList):
    for i, menu in enumerate(menuList):
        option = f"{chr(i+65)}. {menu}"
        print(option)
    option = ",".join([chr(i+65) for i in range(0,len(menuList)-1)]) + f" oder {chr(len(menuList)-1+65)}?"
    a = input(option)
    return a

# Zeigt eine Menueauswahl an
def showMenuDic(menuDic):
    for k in menuDic:
        option = f"{k}. {menuDic[k]}"
        print(option)
    option = ",".join([k for k in menuDic.keys()][:-1]) + f" oder {[k for k in menuDic.keys()][-1]}?"
    a = input(option)
    return a
