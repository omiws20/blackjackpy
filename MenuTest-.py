
menuDic = {"S":"Spiel starten", "D":"Deal", "H":"Hit", "S":"Stand", "Q":"Ende"}

# Zeigt eine Menueauswahl an
def showMenuDic(menuDic):
    for k in menuDic:
        option = f"{k}. {menuDic[k]}"
        print(option)
    option = ",".join([k for k in menuDic.keys()][:-1]) + f" oder {[k for k in menuDic.keys()][-1]}?"
    a = input(option)
    return a

print(showMenuDic(menuDic))