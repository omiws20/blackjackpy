#!/usr/bin/env python3
# GUI-Client mit tkInter für BlackJackNet
# Die Besonderheit ist, dass der komplette Spielzustand auf dem Server verwaltet wird
# Erstellt: 02/11/20, Letzte Aktualisierung: 12/12/20

from tkinter import *
from tkinter import messagebox
import time
from os import path,listdir
from datetime import datetime
import socket
import json
from enum import Enum

# Vergroesserungsfaktor fuer die Spielkarten-Bitmaps (nur provisorisch)
cardList = None
scaleFactor = 5
valueDealer = 0
valuePlayer = 0
gameOver = False

HOST = "127.0.0.1"
PORT = 64000
BUFSIZE = 2048

# Statische Variablen
cardsPath = path.join(path.dirname(__file__), "cards")
defaultCardname = "green_back.png"
defaultCardpath = path.join(cardsPath, defaultCardname)

cardValues = {"One": 1, "Two": 2, "Three": 3, "Four": 4, "Five": 5, "Six": 6,
              "Seven": 7, "Eight": 8, "Nine": 9,"Ten": 10,"Jack": 10,
              "Queen": 10, "King": 10, "Ace": 10}

class gameState(Enum):
    OK = 0
    DealerBlackJack = 1
    PlayerBlackJack = 2
    DealerWin = 3
    PlayerWin = 4
    DealerLoose= 5
    PlayerLoose = 6
    Push = 7

# Der obligatorische Logger
class logger:

    def __init__(self):
        self.logPath = path.join(path.dirname(__file__), "pyblackjackGUIClient.log")
    
    def logInfo(self, msgText):
        with open(self.logPath, mode="a", encoding="utf8") as fh:
            timeStamp = datetime.today().strftime("%H:%M")
            fh.write(f"{timeStamp}: *** {msgText}\n")

    def logError(self, msgText):
        with open(self.logPath, mode="a", encoding="utf8") as fh:
            timeStamp = datetime.today().strftime("%H:%M")
            fh.write(f"{timeStamp}: !!! {msgText}\n")

class card:

    # suit = Club, Diamond, Hearts Spade
    def __init__(self,Index,Value,Name,Suit,ImgPath):
        self.index = Index
        self.value = Value
        self.suit = Suit
        self.name = Name
        self.imgPath = ImgPath

def getStatus(status):
    return list(gameState)[status].name

# Definiert das Fenster der Anwendung
class Window(Frame):

    def __init__(self, root=None):
        Frame.__init__(self, root)
        self.root = root
        self.cavDealerList = []
        self.cavPlayerList = []
        self.imgDealerList = []
        self.imgPlayerList = []

        # Logger initialisieren
        self.logger = logger()

        self.gameCount = 0

        # Bilder einlesen
        self.cardList = self.readCards()

        lblTitel = Label(master=root, text="BlackJack-Casino v0.7", font = "Helvetica 36 bold", fg="red")
        # Nummerierung der Zeilen, Spalten beginnt bei 0
        lblTitel.grid(row=0,column=0)
        fraButtons = Frame(master=root)
        fraButtons.grid(row=1,column=0)
        btnConnect = Button(master=fraButtons, text="Connect", width=16, command=self.connect)
        btnConnect.pack(side=LEFT)
        btnDeal = Button(master=fraButtons, text="Deal", width=16, command=self.deal)
        btnDeal.pack(side=LEFT)
        btnHit = Button(master=fraButtons, text="Hit", width=16, command=self.hit)
        btnHit.pack(side=LEFT)
        btnStand = Button(master=fraButtons, text="Stand", width=16, command=self.stand)
        btnStand.pack(side=LEFT)
        btnQuit = Button(master=fraButtons, text="Quit", width=16, command=self.quit)
        btnQuit.pack(side=LEFT)

        self.frmDealerCards = Frame(master=root)
        self.frmDealerCards.grid(row=2, column=0, sticky=W)

        self.frmPlayerCards = Frame(master=root)
        self.frmPlayerCards.grid(row=3, column=0, sticky=W)

        self.svDealerValue = StringVar(master=root)
        self.svDealerValue.set("0")
        self.lblDealerValue = Label(master=root, textvariable=self.svDealerValue, font="Helvetica 20", fg="green")
        self.lblDealerValue.grid(row=4, column=0, sticky=SW)

        self.svPlayerValue = StringVar(master=root)
        self.svPlayerValue.set("0")
        self.lblPlayerValue = Label(master=root, textvariable=self.svPlayerValue, font="Helvetica 20", fg="blue")
        self.lblPlayerValue.grid(row=4, column=1,sticky=SW)

        self.svStatus = StringVar()
        self.svStatus.set("Status...")
        lblStatus = Label(master=root, textvariable=self.svStatus, font="Helvetica 16", fg="red")
        lblStatus.grid(row=5, column=0,sticky=W)

        # Spalten und Zeilen sollen mehr Platz bekommen
        # Bei nur einer Spalte nicht erforderlich?
        root.columnconfigure(0, weight=1)
        # master.rowconfigure(1, weight=1)
        root.rowconfigure(2, weight=1)
        root.rowconfigure(3, weight=1)

    # Liest alle Bitmapdateien aus einem Verzeichnis ein und gibt eine Liste mit Card-Objekten zurueck
    def readCards(self):
        imgPaths = [path.join(cardsPath, f) for f in listdir(cardsPath) if path.splitext(f)[1] == ".png" and f != defaultCardname]
        cardList = []
        for i, imgPath in enumerate(imgPaths):
            name = path.splitext(path.basename(imgPath))[0]
            value = cardValues[name.split("_")[0]]
            suit = name.split("_")[1]
            cardList.append(card(i,value,name,suit,imgPath))
        return cardList

    def displayCards(self,hideFirstCard=False):
        # Alle frame-Widgets löschen
        for widget in self.frmDealerCards.winfo_children() + self.frmPlayerCards.winfo_children():
            widget.destroy()
        self.cavDealerList = []
        self.cavPlayerList = []
        self.imgDealerList = []
        self.imgPlayerList = []
        # Alle Dealer-Karten anzeigen
        for i,card in enumerate(self.dealerCards):
            cavPic = Canvas(self.frmDealerCards, width=160, height=220)
            cavPic.pack(side=LEFT)
            self.cavDealerList.append(cavPic)
            try:
                localCard = [c for c in self.cardList if c.name == card["name"]][0]
                imgPath = localCard.imgPath
                imgNeu = PhotoImage(file=imgPath).subsample(scaleFactor)
                self.imgDealerList.append(imgNeu)
                if i == 0:
                    # Das erste Image muss fuer die spaetere Anzeige zwischengespeichert werden
                    self.hiddenImage = imgNeu.copy()
                    # Hintergrundbild festlegen
                    imgNeu = PhotoImage(file=defaultCardpath).subsample(scaleFactor)
                    self.backcardImage = imgNeu
                cavPic.create_image(0, 0, anchor=NW, image=imgNeu)
            except:
                logMsg = f"!!! Fehler bei Karte {card['name']}"
                self.logger.logInfo(logMsg)
                print(logMsg)

        # Alle Player-Karten anzeigen
        for card in self.playerCards:
            cavPic = Canvas(self.frmPlayerCards, width=160, height=220)
            cavPic.pack(side=LEFT)
            self.cavPlayerList.append(cavPic)
            localCard = [c for c in self.cardList if c.name == card["name"]][0]
            imgPath = localCard.imgPath
            imgNeu = PhotoImage(file=imgPath).subsample(scaleFactor)
            self.imgPlayerList.append(imgNeu)
            cavPic.create_image(0, 0, anchor=NW, image=imgNeu)

    def connect(self):
        self.gameSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.gameSocket.connect((HOST, PORT))
        self.gameSocket.sendall(b"start:1000")
        resp = self.gameSocket.recv(BUFSIZE)
        jsonObj = json.loads(resp.decode("utf8"))
        header = jsonObj["header"]
        logMsg = "Rückgabe start:" + header
        self.logger.logInfo(logMsg)
        self.svStatus.set(header)

    def deal(self):
        # Dealer soll Karten liefern - zwei für den Dealer und zwei für den Player
        # Wurde bereits gespielt (nur provisorisch)
        if self.gameCount > 0:
            self.gameSocket.sendall(b"deal:initGame")
        else:
            self.gameSocket.sendall(b"deal")
        resp = self.gameSocket.recv(BUFSIZE)
        # Server sendet die zwei Dealer- und zwei Player-Karten als Json-Text
        # card = int.from_bytes(resp, byteorder="little")
        jsonObj = json.loads(resp.decode("utf8"))
        logMsg = "Rückgabe deal: " + jsonObj["header"]
        self.logger.logInfo(logMsg)

        # Game Counter erhöhen, damit Deal mehrfach ausgeführt werden kann
        self.gameCount += 1

        self.dealerCards = jsonObj["body"][0]
        self.playerCards = jsonObj["body"][1]
        dealerValue = jsonObj["body"][2]
        playerValue = jsonObj["body"][3]
        self.svDealerValue.set(dealerValue)
        self.svPlayerValue.set(playerValue)

        self.displayCards(hideFirstCard=True)

        # Gibt es einen Blackjack?
        status = int(jsonObj["state"])
        statusText = getStatus(status)
        logMsg = f"Spielstatus: {statusText}"
        self.logger.logInfo(logMsg)
        self.svStatus.set(statusText)

        # Ist das Spiel zu Ende? Dann verdeckte Dealer-Karte anzeigen
        if status > 0:
            cavPic = self.cavDealerList[0]
            cavPic.create_image(0, 0, anchor=NW, image=self.hiddenImage)

    def hit(self):
        # Dealer soll nächste Player-Karte liefern oder Gewinn/Verloren-Info
        self.gameSocket.send(b"hit")
        resp = self.gameSocket.recv(BUFSIZE)
        jsonObj = json.loads(resp.decode("utf8"))
        logMsg = "Rückgabe hit: " + jsonObj["header"]
        self.logger.logInfo(logMsg)
        self.dealerCards = jsonObj["body"][0]
        self.playerCards = jsonObj["body"][1]
        dealerValue = jsonObj["body"][2]
        playerValue = jsonObj["body"][3]
        self.svDealerValue.set(dealerValue)
        self.svPlayerValue.set(playerValue)

        self.displayCards()

        # Spielstatus aktualisieren
        status = int(jsonObj["state"])
        statusText = getStatus(status)
        self.svStatus.set(statusText)
        logMsg = f"Spielstatus: {statusText}"
        self.logger.logInfo(logMsg)

        # Ist das Spiel zu Ende? Dann verdeckte Dealer-Karte anzeigen
        if status > 0:
            cavPic = self.cavDealerList[0]
            cavPic.create_image(0, 0, anchor=NW, image=self.hiddenImage)

    def stand(self):
        self.gameSocket.sendall(b"stand")
        resp = self.gameSocket.recv(BUFSIZE)
        jsonObj = json.loads(resp.decode("utf8"))
        logMsg = "Rückgabe stand: " + jsonObj["header"]
        self.logger.logInfo(logMsg)
        self.dealerCards = jsonObj["body"][0]
        self.playerCards = jsonObj["body"][1]
        dealerValue = jsonObj["body"][2]
        self.svDealerValue.set(dealerValue)
        playerValue = jsonObj["body"][3]
        self.svPlayerValue.set(playerValue)

        self.displayCards()

        # Spielstatus aktualisieren
        status = int(jsonObj["state"])
        statusText = getStatus(status)
        self.svStatus.set(statusText)
        logMsg = f"Spielstatus: {statusText}"
        self.logger.logInfo(logMsg)

        # Verdeckte Dealer-Karte anzeigen
        cavPic = self.cavDealerList[0]
        cavPic.create_image(0, 0, anchor=NW, image=self.hiddenImage)

    def quit(self):
        try:
            self.gameSocket.send(b"quit")
        except:
            logMsg = "!!! Fehler beim Aufruf der quit-Methode !!!"
            print(logMsg)
            self.logger.logInfo(logMsg)
        # Rückgabe ist Spiel-Statistik
        resp = self.gameSocket.recv(BUFSIZE)
        jsonObj = json.loads(resp.decode("utf8"))
        header = jsonObj["header"]
        logMsg = "Rückgabe quit: " + header
        self.logger.logInfo(logMsg)
        self.gameSocket.close()
        self.master.destroy()

root = Tk()
root.title("BlackJack 0.7")
root.geometry("900x600")
win = Window(root)
win.mainloop()