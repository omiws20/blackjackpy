#!/usr/bin/env python3
# # Ein BlackJack-Spiel gegen den Computer mit tkinter
# Erstellt: 24/10/20, Letzte Aktualisierung: 27/10/20
# Mit Klassen aus dem UML-Diagramm und daher etwas anspruchsvoller durch die klare Trennung

# coding: utf8

# import PyBlackJack
from PyBlackJack import *

from tkinter import *
from tkinter import messagebox
import time

# Vergroesserungsfaktor fuer die Spielkarten-Bitmaps (nur provisorisch)
cardList = None
scaleFactor = 5
valueDealer = 0
valuePlayer = 0
gameOver = False

# Definiert das Fenster der Anwendung
class Window(Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.cavDealerList = []
        self.cavPlayerList = []
        self.imgDealerList = []
        self.imgPlayerList = []

        lblTitel = Label(master, text="BlackJack-Casino v0.5",font = "Helvetica 36 bold",fg="red")
        # Nummerierung der Zeilen, Spalten beginnt bei 0
        lblTitel.grid(row=0,column=0)
        fraButtons = Frame(master)
        fraButtons.grid(row=1,column=0)
        btnDeal = Button(fraButtons, text="Deal", width=16, command=self.deal)
        btnDeal.pack(side=LEFT)
        btnHit = Button(fraButtons, text="Hit", width=16, command=self.hit)
        btnHit.pack(side=LEFT)
        btnStand = Button(fraButtons, text="Stand", width=16, command=self.stand)
        btnStand.pack(side=LEFT)
        btnQuit = Button(fraButtons, text="Quit", width=16, command=self.quit)
        btnQuit.pack(side=LEFT)

        self.frmDealerCards = Frame(master)
        self.frmDealerCards.grid(row=2, column=0,sticky=W)

        self.frmPlayerCards = Frame(master)
        self.frmPlayerCards.grid(row=3, column=0,sticky=W)

        self.lblDealerValue = Label(master, text="0", font="Helvetica 20",fg="green")
        self.lblDealerValue.grid(row=4, column=0,sticky=SW)

        self.lblPlayerValue = Label(master, text="0", font="Helvetica 20",fg="blue")
        self.lblPlayerValue.grid(row=4, column=1,sticky=SW)

        # Spalten und Zeilen sollen mehr Platz bekommen
        # Bei nur einer Spalte nicht erforderlich?
        master.columnconfigure(0, weight=1)
        # master.rowconfigure(1, weight=1)
        master.rowconfigure(2, weight=1)
        master.rowconfigure(3, weight=1)

        self.currentLogger = Logger()
        self.currentGame = Game(self.currentLogger)
        self.currentDeck = Deck(self.currentGame.cardList)
        self.currentDealer = Dealer(self.currentGame, self.currentDeck, "Joe")
        self.currentPlayer = Player(self.currentGame, self.currentDealer, "Pemo")

    def deal(self):
        global gameOver, playerValue
        gameOver = False
        # Dealer- und Player-Hand zurücksetzen
        self.currentDealer.cardHand = []
        self.currentPlayer.cardHand = []
        self.currentGame.Start()
        # Alle angezeigten Karten als Canvas-Widgets des Dealers und des Players entfernen
        self.cavDealerList.clear()
        self.cavPlayerList.clear()
        self.imgDealerList.clear()
        self.imgPlayerList.clear()

        # Alle Widgets aus den beiden Frames entfernen
        for widget in self.frmDealerCards.winfo_children() + self.frmPlayerCards.winfo_children():
            widget.destroy()

        # Ist das auch erfoerderlich? Kann wahrscheinlich wieder raus
        for cav in self.cavDealerList + self.cavPlayerList:
            # cav.pack_forget()
            cav.destroy()

        # Zwei Karten fuer den Dealer, eine wird verdeckt angezeigt
        for _ in range(0,2):
            newCard = self.currentDealer.newCard()
            cavPic = Canvas(self.frmDealerCards,width=160,height=220)
            cavPic.pack(side=LEFT)
            self.cavDealerList.append(cavPic)
            imgNeu = PhotoImage(file=newCard.imgPath).subsample(scaleFactor)
            self.imgDealerList.append(imgNeu)
            if _ == 0:
                # Das erste Image muss fuer die spaetere Anzeige zwischengespeichert werden
                imgNeu = PhotoImage(file=Game.defaultCardpath).subsample(scaleFactor)
                self.backcardImage = imgNeu
            cavPic.create_image(0,0,anchor=NW,image=imgNeu)

        # Zwei Karten fuer den Spieler
        for _ in range(0,2):
            newCard = self.currentPlayer.hit()
            cavPic = Canvas(self.frmPlayerCards,width=160,height=220)   
            cavPic.pack(side=LEFT)
            self.cavPlayerList.append(cavPic)
            img = PhotoImage(file=newCard.imgPath).subsample(scaleFactor)
            cavPic.create_image(0,0,anchor=NW,image=img)
            self.imgPlayerList.append(img)

        # Aktuellen Wert der Dealer-Karten berechnen
        dealerValue = self.currentDealer.getValue()
        self.lblDealerValue["text"] = "Dealer: " + str(dealerValue)
        
        # Aktuellen Wert der Player-Karten berechnen
        playerValue = self.currentPlayer.getValue()
        self.lblPlayerValue["text"] = "Player: " + str(playerValue)

        # Hat der Dealer oder der Player einen BlackJack oder gibt es einen Push?
        if dealerValue == 21 and playerValue == 21:
            messagebox.showinfo(title="PyBlackJack", message="Push - BlackJack fuer Spieler und Dealer!")
            gameOver = True
        elif dealerValue == 21:
            messagebox.showinfo(title="PyBlackJack", message="BlackJack - die Bank gewinnt!")
            gameOver = True
        elif playerValue == 21:
            messagebox.showinfo(title="PyBlackJack", message="BlackJack - die Bank gewinnt!")
            gameOver = True
        if gameOver:
            # Verzoegerung ueber sleep() nicht ganz optimal, da blockierend
            time.sleep(0.4)
            self.onUpdate()
            # Verdeckte Karte des Dealers aufdecken
            cav = self.cavDealerList[0]
            img = self.imgDealerList[0]
            # Optional
            cav.delete(ALL)
            # mit itemconfig geht es einfach nicht?
            # cav.itemconfig(imgBack,image=img)
            cav.create_image(0,0,anchor=NW,image=img)

    # Eine weitere Karte fuer den Spieler
    def hit(self):
        global gameOver
        # Ein weiteres Canvas anlegen
        cavPic = Canvas(self.frmPlayerCards,width=160,height=220)   
        cavPic.pack(side=LEFT)
        self.cavPlayerList.append(cavPic)
        newCard = self.currentPlayer.hit()
        img = PhotoImage(file=newCard.imgPath).subsample(scaleFactor)
        cavPic.create_image(0,0,anchor=NW,image=img)
        self.imgPlayerList.append(img)
        # Aktuellen Wert der Player-Karten berechnen
        playerValue = self.currentPlayer.getValue()
        self.lblPlayerValue["text"] = "Player: " + str(playerValue)
        # Hat der Player gewonnen oder verloren?
        if playerValue == 21:
            messagebox.showinfo(title="PyBlackJack", message="Der Spieler hat gewonnen!")
            gameOver = True
        elif playerValue > 21:
            messagebox.showinfo(title="PyBlackJack", message="Der Spieler hat verloren!")
            gameOver = True
        if gameOver:
            # Verzoegerung ueber sleep() nicht ganz optimal, da blockierend
            time.sleep(0.4)
            self.onUpdate()
            img = self.imgDealerList[0]
            cav = self.cavDealerList[0]
            cav.create_image(0,0,anchor=NW,image=img)

    def onUpdate(self):
        self.after(1000, self.onUpdate)

    # Der Spieler ist fertig, der Dealer ist an der Reihe 
    def stand(self):
        global gameOver
        dealerValue = self.currentDealer.getValue()
        playerValue = self.currentPlayer.getValue()
        # Hat der Dealer bereits gewonnen?
        if dealerValue > playerValue:
            messagebox.showinfo(title="PyBlackJack", message="Die Bank gewinnt!")
            gameOver = True
        elif dealerValue == playerValue:
            messagebox.showinfo(title="PyBlackJack", message="Keiner gewinnt - Push!")
            gameOver = True
        # Weitere Karten ziehen bis das Spiel zu Ende ist
        while not gameOver:
            newCard = self.currentDealer.newCard()
            cavPic = Canvas(self.frmDealerCards,width=160,height=220)
            cavPic.pack(side=LEFT)
            self.cavDealerList.append(cavPic)
            img = PhotoImage(file=newCard.imgPath).subsample(scaleFactor)
            cavPic.create_image(0,0,anchor=NW,image=img)
            self.imgDealerList.append(img)
            # Aktuellen Wert der Dealer-Karten berechnen
            dealerValue = self.currentDealer.getValue()
            self.lblDealerValue["text"] = "Dealer: " + str(dealerValue)
            # Hat der Dealer gewonnen oder verloren?
            if dealerValue == 21:
                messagebox.showinfo(title="PyBlackJack", message="Die Bank gewinnt!")
                gameOver = True
            elif dealerValue > 21:
                messagebox.showinfo(title="PyBlackJack", message="Die Bank verloren!")
                gameOver = True
            elif dealerValue > playerValue:
                messagebox.showinfo(title="PyBlackJack", message="Die Bank gewinnt!")
                gameOver = True
            elif playerValue == dealerValue:
                messagebox.showinfo(title="PyBlackJack", message="Keiner gewinnt - Push!")
                gameOver = True
            # Verzoegerung ueber sleep() nicht ganz optimal, da blockierend
            time.sleep(0.4)
            self.onUpdate()
        # Verdeckte Dealer-Karte aufdecken
        cav = self.cavDealerList[0]
        img = self.imgDealerList[0]
        # Optional
        cav.delete(ALL)
        cav.create_image(0,0,anchor=NW,image=img)
        # Verzoegerung ueber sleep() nicht ganz optimal, da blockierend
        time.sleep(0.4)
        self.onUpdate()

    # Spiel beenden
    def quit(self):
        self.master.destroy()

root = Tk()
root.title("BlackJack 0.5")
root.geometry("900x600")
win = Window(root)
win.mainloop()
