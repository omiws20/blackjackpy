# Liste mit Objekten nach JSON konvertieren
from datetime import datetime
import json
from json import JSONEncoder

class Message(object):

    def __init__(self, Id, Header, Body):
        self.id = Id
        self.header = Header
        self.body = Body

class MsgEncoder(JSONEncoder):

    def default(self, o):
        return o.__dict__

class DateEncoder(JSONEncoder):

        def default(self, obj):
            # if isinstance(obj, (datetime.date, datetime.datetime)):
            if isinstance(obj, datetime):
                return obj.isoformat()

class Game:

    def __init__(self, Name):
        self.Name = Name
        self.Id = 1000
        # self.startTime = datetime.today()
        self.startTime =  json.dumps(datetime.today(), indent=4, cls=DateEncoder)

gameId = 1001
gameList = [Game("Game of gnomes"), Game("Bears in the closet"),Game("Wild orgy")]
header = "Test-1234"
body = gameList
msg = Message(gameId, header, body)
jsonText = json.dumps(msg, indent=4, cls=MsgEncoder,default = lambda x: x.__dict__)

print(jsonText)