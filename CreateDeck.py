# Anlegen eines Decks mit 52 Karten

from enum import Enum

dicCards = {"One":1,"Two":2,"Three":3,"Four":4,"Five":5,"Six":6,"Seven":7,"Eight":8,"Nine":9,"Ten":10,
         "Jack":10,"Queen":10,"King":10,"Ace":10}

class Color(Enum):
    Heart = 0
    Diamond = 1
    Spade = 2
    Club = 3

class Card:

    def __init__(self, Color, Value, Valuename):
        self.Color = Color
        self.Value = Value
        self.Valuename = Valuename

    def __repr__(self):
        return f"{self.Valuename}"

cards = []

for col in Color.__dict__.keys():
    if not col.startswith("_"):
        for c in dicCards.items():
            cards.append(Card(col, c[1], f"{col}_{c[0]}"))

for c in cards:
    print(c)