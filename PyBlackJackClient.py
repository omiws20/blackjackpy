# Stellt die Verbindung mit einem BlackJack-Server her
# Erstellt: 27/10/20, Letzte Aktualisierung: 12/11/20

# Die Idee ist, dass der Client immer den kompletten Spielzustand erhält und keinen Zustand speichert
# TODO: Die Spielkarten werden als Bitmaps übertragen? Eventuell am Anfang einmal?
import socket
import json

from PyBlackJackNet import *
from BlackJackHelper import *

HOST = "127.0.0.1"
PORT = 64000
BUFSIZE = 2048

menuDic = {"G":"Spiel starten", "D":"Deal", "H":"Hit", "S":"Stand", "Q":"Ende"}

playerCards = []
dealerCards = []

# Umsetzen des Status-Wertes in einen Text
def getStatus(status):
    statusDic = {1:"Dealer hat BlackJack", 2:"Player hat BlackJack", 3:"Dealer gewinnt", 4:"Player gewinnt",
     5:"Dealer verliert", 6:"Player verliert", 7:"Es gibt einen Push"}
    statusText = statusDic[status] if statusDic.get(status) else "OK"
    return statusText

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sh:
    sh.connect((HOST, PORT))
    abbruch = False
    while not abbruch:
        a = showMenuDic(menuDic)
        if a == "G":
            # Spiel starten über => start
            # Server sendet Game-ID und Startzeit zurück
            sh.sendall(b"start:100")
            resp = sh.recv(BUFSIZE)
            jsonObj = json.loads(resp.decode("utf8"))
            print("Rückgabe start:" + jsonObj["header"])
        elif a == "D":
            # Dealer soll Karten liefern - zwei für den Dealer und zwei für den Player
            sh.send(b"deal")
            resp = sh.recv(BUFSIZE)
            # Server sendet die zwei Dealer- und zwei Player-Karten als Json-Text
            # card = int.from_bytes(resp, byteorder="little")
            jsonObj = json.loads(resp.decode("utf8"))
            print("Rückgabe deal: " + jsonObj["header"])
            dealerCards = jsonObj["body"][0]
            playerCards = jsonObj["body"][1]
            cardValues = [c["value"] for c in dealerCards]
            cardSum = sum(cardValues)
            # Eventuell gibt es eine kürzere Schreibweise, um aus einer int-Liste eine str-Liste zu machen
            dealerInfo = ",".join([c["name"] for c in dealerCards]) + " (Summe=" + str(cardSum) + ")"
            print("Dealer cards: " + dealerInfo)
            # playerCardNumbers = ",".join([str(c["value"]) for c in playerCards])
            cardValues = [c["value"] for c in playerCards]
            cardSum = sum(cardValues)
            playerInfo = ",".join([c["name"]  for c in playerCards]) + " (Summe=" + str(cardSum) + ")"
            print("Player cards: " + playerInfo)
            # Gibt es einen Blackjack?
            status = int(jsonObj["status"])
            print(f"Spielstatus: {getStatus(status)}")
            if status > 0:
                abbruch = True
        elif a == "H":
            # Dealer soll nächste Player-Karte liefern oder Gewinn/Verloren-Info
            # Spiellogik liegt beim Server
            sh.send(b"hit")
            resp = sh.recv(BUFSIZE)
            jsonObj = json.loads(resp.decode("utf8"))
            # card = int.from_bytes(resp, byteorder="little")
            # print("Rückgabe hit: " + card)
            print("Rückgabe hit: " + jsonObj["header"])
            dealerCards = jsonObj["body"][0]
            playerCards = jsonObj["body"][1]
            cardValues = [c["value"] for c in dealerCards]
            cardSum = sum(cardValues)
            # Eventuell gibt es eine kürzere Schreibweise, um aus einer int-Liste eine str-Liste zu machen
            dealerInfo = ",".join([c["name"] for c in dealerCards]) + " (Summe=" + str(cardSum) + ")"
            print("Dealer cards: " + dealerInfo)
            cardValues = [c["value"] for c in playerCards]
            cardSum = sum(cardValues)
            playerInfo = ",".join([c["name"]  for c in playerCards]) + " (Summe=" + str(cardSum) + ")"
            print("Player cards: " + playerInfo)
            # Ist das Spiel zu Ende?
            status = int(jsonObj["status"])
            print(f"Spielstatus: {getStatus(status)}")
            if status > 0:
                abbruch = True
        elif a == "S":
            # Dealer soll Spiel beenden und liefert alle Dealer-Karten und eine Gewinn/Verloren-Info zurück
            sh.send(b"stand")
            resp = sh.recv(BUFSIZE)
            jsonObj = json.loads(resp.decode("utf8"))
            print("Rückgabe stand: " + jsonObj["header"])
            dealerCards = jsonObj["body"][0]
            playerCards = jsonObj["body"][1]
            cardValues = [c["value"] for c in dealerCards]
            cardSum = sum(cardValues)
            dealerInfo = ",".join([c["name"] for c in dealerCards]) + " (Summe=" + str(cardSum) + ")"
            print("Dealer cards: " + dealerInfo)
            cardValues = [c["value"] for c in playerCards]
            cardSum = sum(cardValues)
            playerInfo = ",".join([c["name"]  for c in playerCards]) + " (Summe=" + str(cardSum) + ")"
            print("Player cards: " + playerInfo)
            # Ist das Spiel zu Ende?
            status = int(jsonObj["status"])
            print(f"Spielstatus: {getStatus(status)}")
            if status > 0:
                abbruch = True
            # card = int.from_bytes(resp, byteorder="little")
            # print(card)
            # print("Rückgabe stand: " + card)
        elif a == "Q":
            # Spiel wird beendet
            sh.send(b"quit")
            # Rückgabe ist Spiel-Statistik
            resp = sh.recv(BUFSIZE)
            jsonObj = json.loads(resp.decode("utf8"))
            # print("Rückgabe quit: " + resp.decode("utf8"))
            print("Rückgabe quit: " + jsonObj["header"])
            abbruch = True
        else:
            print("!!! Unbekannte Auswahl !!!")

