#!/usr/bin/env python3
# Version 2 des PyBlackJack-Netzwerkspiels
# Erstellt: 14/12/20

from datetime import datetime
from os import path, listdir
import random
import socket
import json
from json import JSONEncoder
import uuid

from enum import Enum

class Suit(Enum):
    Heart = 0
    Diamond = 1
    Spade = 2
    Club = 3

cardValues = {"Two":2,"Three":3,"Four":4,"Five":5,"Six":6,
              "Seven":7,"Eight":8,"Nine":9,"Ten":10,
              "Jack":10,"Queen":10,"King":10,"Ace":10}

# Ableiten von str, damit Umwandlung in JSON später einfacher wird
class gameState(str, Enum):
    OK = "0"
    DelaerBlackJack = "1"
    PlayerBlackJack = "2"
    DealerWin = "3"
    PlayerWin = "4"
    DealerLoose= "5"
    PlayerLoose = "6"
    Push = "7"

class message(object):

    def __init__(self, Id, Header, Body):
        self.id = Id
        self.header = Header
        self.body = Body
        self.state = 0

class msgEncoder(JSONEncoder):

    def default(self, o):
        return o.__dict__

class datetimeEncoder(JSONEncoder):

    def default(self, obj):
        # if isinstance(obj, (datetime.date, datetime.datetime)):
        if isinstance(obj, datetime):
            return obj.isoformat()

# Definiert den (obligatorischen) Logger
class logger:

    def __init__(self):
        self.logPath = path.join(path.dirname(__file__), "pyblackjackNet.log")

    def logInfo(self, msgText):
        with open(self.logPath, mode="a", encoding="utf8") as fh:
            timeStamp = datetime.today().strftime("%H:%M")
            fh.write(f"{timeStamp}: *** {msgText}\n")

    def logError(self, msgText):
        with open(self.logPath, mode="a", encoding="utf8") as fh:
            timeStamp = datetime.today().strftime("%H:%M")
            fh.write(f"{timeStamp}: !!! {msgText}\n")

# Definiert eine Karte
class card:

    def __init__(self, Index, Name, Suit, Value):
        self.index = Index
        self.name = Name
        self.suit = Suit
        self.value = Value

    def __repr__(self):
        return f"{self.name} of {self.suit} ({self.value})"

class classEncoder(JSONEncoder):

    def default(self, o):
        return o.__dict__

# Definiert ein Kartendeck mit 52 Karten
class deck:

    def createDeck(self):
        cards = []
        i = 0
        for col in Suit.__dict__.keys():
            if not col.startswith("_"):
                for c in cardValues.items():
                    i += 1
                    cards.append(card(i, c[0] + "_" + col, col, c[1]))
        return cards

    def __init__(self):
        self.cards = self.createDeck()

# Definiert den Dealer des Spiels
class dealer:

    def __init__(self, Game, Deck, Name):
        self.game = Game
        self.name = Name
        self.deck = Deck
        self.logger = Game.logger

    def getHandValue(self):
        value = self.game.Calculate(self.game.dealerCards)
        return value

# Definiert einen Spieler
class player:

    def __init__(self, Game, Name, Bet):
        self.game = Game
        self.name = Name
        self.bet = Bet
        self.logger = Game.logger

    # Dealer gibt Player eine Karte
    def hit(self):
        newCard = self.game.newCard(False)
        logMsg = f"Calling hit() for Player - new card={newCard}"
        self.logger.logInfo(logMsg)
        return newCard

    def getValue(self):
        value = self.game.Calculate(self.game.playerCards)
        return value

# Definiert ein Spiel
class game:

    def __init__(self,Logger):
        self.logger = Logger
        self.gameId = uuid.uuid4()

    def start(self,Bet):
        self.startTime = datetime.today()
        # Alle Listen initialisieren
        self.dealerCards = []
        self.playerCards = []
        msgText = f"Game Id={self.gameId} started at {self.startTime:%H:%M}"
        self.logger.logInfo(msgText)
        self.currentDeck = deck()
        self.currentDealer = dealer(self, self.currentDeck, "The Dealer")
        self.currentPlayer = player(self, "Petro", Bet)

   # Berechnet den Wert eines Kartenstapels
    def calculateCards(self, Cards):
        value = 0
        for card in Cards:
            value += card.value
        # Muessen die Asse anders gezaehlt werden?
        # Die Idee ist, bei > 10 Punkten zählt ein Ass nur 1 Punkt
        if value > 10:
            asse = [c for c in Cards if c.name == "Ace"]
            for _ in asse:
                # Pro Ass 10 Punkte abziehen, aus Ass+Koenig+9 = 30 wird so 20
                value -= 10
        return value

    # Gibt eine weitere Karte zurück
    def newCard(self, dealerCard):
        exitFlag = False
        while not exitFlag:
            z = random.randint(0, len(self.currentDeck.cards) - 1)
            if z not in [c.index for c in self.dealerCards + self.playerCards]:
                card = self.currentDeck.cards[z]
                # Ist die Karte für den Dealer?
                if dealerCard:
                    self.dealerCards.append(card)
                else:
                    self.playerCards.append(card)
                exitFlag = True
        name = "Dealer" if dealerCard else "Player"
        logMsg = f"New card {card} for {name}"
        self.logger.logInfo(logMsg)
        return card


# Definiert eine Spielsession
class gameSession:

    # Statische Variablen
    cardsPath = path.join(path.dirname(__file__), "Cards")
    defaultCardname = "green_back.png"
    defaultCardpath = path.join(cardsPath, defaultCardname)

    HOST = "127.0.0.1"
    PORT = 64000
    BUFSIZE = 2048

    def __init__(self, Logger):
        self.logger = Logger

    def connect(self):
        global conn
        logMsg = "*** Der PyBlackJack Server V2 wartet auf eine Verbindung ***"
        print(logMsg)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sh:
            sh.bind((gameSession.HOST, gameSession.PORT))
            sh.listen()
            conn, addr = sh.accept()
            logMsg = "*** Verbindung wurde hergestellt ***"
            print(logMsg)
            self.logger.logInfo(logMsg)
            exitFlag = False
            with conn:
                logMsg = "Connected"
                self.logger.logInfo(logMsg)
                while not exitFlag:
                    data = conn.recv(256)
                    # Bytes in Text konvertieren und alle Leerzeichen abschneiden
                    cmd = data.decode("utf8").strip()
                    print(f"+++ -> {cmd}")
                    # Abbrechen, wenn keine Daten
                    if not cmd:
                        exitFlag = True
                    # Parameter extrahieren, wenn vorhanden, z.B. start:100
                    cmdPara = ""
                    if ":" in cmd:
                        cmd, cmdPara = cmd.split(":")
                    # Alle Kommandos abfragen
                    if cmd == "start":
                        logMsg = "start-command"
                        self.logger.logInfo(logMsg)
                        # Spiel starten
                        self.playerBet = int(cmdPara)
                        # Neues Spiel initialisieren
                        self.game = game(self.logger)
                        # Spiel mit Einsatz starten
                        self.game.start(self.playerBet)
                        header = f" Game mit Id={self.game.gameId} und Einsatz={self.game.currentPlayer.bet} gestartet"
                        body = None
                        msg = message(self.game.gameId, header, body)
                        jsonText = json.dumps(msg, indent=4, cls=msgEncoder)
                        conn.sendall(bytes(jsonText,encoding="utf8"))
                    elif cmd == "deal":
                        # Spiel wird ein weiteres Mal gestartet
                        if cmdPara == "initGame":
                            # Neues Spiel initialisieren
                            self.game = game(self.logger)
                            # Spiel mit Einsatz starten
                            self.game.start(self.playerBet)
                        # Beim ersten Deal können weder Dealer noch Player über 21 kommen, da das zweite Ass mit 1 gezählt wird
                        # Die einzige Abbruch-Situation ist daher ein BlackJack, also Bild + Ass
                        logMsg = "deal-command"
                        self.logger.logInfo(logMsg)
                        header = "Dealer gibt sich und dem Player zwei Karten"
                        newCard = self.game.newCard(True)
                        logMsg = f"+++ -> Dealer gibt sich eine Karte: {newCard}"
                        print(logMsg)
                        self.logger.logInfo(logMsg)
                        newCard = self.game.newCard(True)
                        logMsg = f"+++ -> Dealer gibt sich eine Karte: {newCard}"
                        print(logMsg)
                        self.logger.logInfo(logMsg)
                        # Hat der Dealer einen Black Jack?
                        dealerValue = self.game.calculateCards(self.game.dealerCards)
                        logMsg = f"+++ -> Aktueller Dealer-Value: {dealerValue}"
                        print(logMsg)
                        self.logger.logInfo(logMsg)
                        if dealerValue == 21:
                            currentState = gameState.DealerBlackJack
                            print(f"+++ -> Der Dealer hat einen BlackJack")
                        else:
                            # Player erhält zwei Karten
                            newCard = self.game.newCard(False)
                            logMsg = f"+++ -> Dealer gibt Player eine Karte: {newCard}"
                            print(logMsg)
                            self.logger.logInfo(logMsg)
                            newCard = self.game.newCard(False)
                            logMsg = f"+++ -> Dealer gibt Player eine Karte: {newCard}"
                            print(logMsg)
                            self.logger.logInfo(logMsg)
                            # Hat der Player einen Black Jack?
                            playerValue = self.game.calculateCards(self.game.playerCards)
                            logMsg = f"+++ -> Aktueller Player-Value: {playerValue}"
                            print(logMsg)
                            self.logger.logInfo(logMsg)
                            if playerValue == 21:
                                currentState = gameState.PlayerBlackJack
                                logMsg = f"+++ -> Der Player hat einen BlackJack"
                                print(logMsg)
                                self.logger.logInfo(logMsg)
                            else:
                                currentState = gameState.OK
                        body = [self.game.dealerCards, self.game.playerCards, dealerValue, playerValue]
                        msg = message(self.game.gameId, header, body)
                        msg.state = currentState
                        jsonText = json.dumps(msg, indent=4, cls=msgEncoder, default = lambda x: x.__dict__)
                        conn.sendall(bytes(jsonText,encoding="utf8"))
                    elif cmd == "hit":
                        logMsg = "hit-command"
                        self.logger.logInfo(logMsg)
                        header = "Dealer gibt Player eine weitere Karte"
                        newCard = self.game.newCard(False)
                        logMsg = f"+++ -> Dealer gibt Player eine Karte: {newCard}"
                        print(logMsg)
                        self.logger.logInfo(logMsg)
                        # Hat der Player gewonnen oder verloren?
                        playerValue = self.game.calculateCards(self.game.playerCards)
                        logMsg = f"+++ -> Aktueller Player-Value: {playerValue}"
                        print(logMsg)
                        self.logger.logInfo(logMsg)
                        if playerValue == 21:
                            currentState = gameState.PlayerWin
                            logMsg = f"+++ -> Der Player hat einen BlackJack"
                            print(logMsg)
                            self.logger.logInfo(logMsg)
                        elif playerValue > 21:
                            currentState = gameState.PlayerLoose
                            logMsg = f"+++ -> Der Player hat verloren"
                            print(logMsg)
                            self.logger.logInfo(logMsg)
                        else:
                            currentState = gameState.OK
                        body = [self.game.dealerCards, self.game.playerCards, dealerValue, playerValue]
                        msg = message(self.game.gameId, header, body)
                        msg.state = currentState
                        jsonText = json.dumps(msg, indent=4, cls=msgEncoder, default=lambda x: x.__dict__)
                        conn.sendall(bytes(jsonText,encoding="utf8"))
                    elif cmd == "stand":
                        logMsg = "stand-command"
                        self.logger.logInfo(logMsg)
                        header = "Dealer gibt sich weitere Karten bis das Spiel zu Ende ist"
                        # Hat der Dealer bereits gewonnen?
                        dealerValue = self.game.calculateCards(self.game.dealerCards)
                        playerValue = self.game.calculateCards(self.game.playerCards)
                        if dealerValue > playerValue:
                            currentState = gameState.DealerWin
                            logMsg = f"+++ -> Der Dealer hat gewonnen"
                            self.logger.logInfo(logMsg)
                            print(logMsg)
                        else:
                            exitFlagStand = False
                            while not exitFlagStand:
                                newCard = self.game.newCard(True)
                                logMsg = f"+++ -> Dealer gibt sich eine Karte: {newCard}"
                                self.logger.logInfo(logMsg)
                                print(logMsg)
                                # Hat der Dealer gewonnen oder verloren?
                                dealerValue = self.game.calculateCards(self.game.dealerCards)
                                logMsg = f"+++ -> Aktueller Dealer-Value: {dealerValue}"
                                self.logger.logInfo(logMsg)
                                print(logMsg)
                                playerValue = self.game.calculateCards(self.game.playerCards)
                                logMsg = f"+++ -> Aktueller Player-Value: {playerValue}"
                                print(logMsg)
                                self.logger.logInfo(logMsg)
                                if dealerValue > 21:
                                    currentState = gameState.DealerLoose
                                    logMsg = f"+++ -> Der Dealer hat verloren"
                                    print(logMsg)
                                    self.logger.logInfo(logMsg)
                                elif dealerValue > playerValue:
                                    currentState = gameState.DealerWin
                                    logMsg = f"+++ -> Der Dealer hat gewonnen"
                                    print(logMsg)
                                    self.logger.logInfo(logMsg)
                                elif dealerValue == 21:
                                    currentState = gameState.DealerWin
                                    logMsg = f"+++ -> Der Dealer hat einen BlackJack"
                                    print(logMsg)
                                    self.logger.logInfo(logMsg)
                                elif dealerValue == playerValue:
                                    currentState = gameState.Push
                                else:
                                    currentState = gameState.OK
                                if currentState != gameState.OK:
                                    exitFlagStand = True
                        body = [self.game.dealerCards, self.game.playerCards, dealerValue, playerValue]
                        msg = message(self.game.gameId, header, body)
                        msg.state = currentState
                        jsonText = json.dumps(msg, indent=4, cls=msgEncoder, default = lambda x: x.__dict__)
                        conn.sendall(bytes(jsonText,encoding="utf8"))
                    elif cmd == "quit":
                        logMsg = "quit-command"
                        self.logger.logInfo(logMsg)
                        header = "Game terminated"
                        body = None
                        msg = message(self.game.gameId, header, body)
                        jsonText = json.dumps(msg, indent=4, cls=msgEncoder)
                        conn.sendall(bytes(jsonText,encoding="utf8"))
                        exitFlag = True
                    else:
                        logMsg = f"Unknown command: {cmd}"
                        self.logger.logInfo(logMsg)

    def disconnect(self):
        global conn
        conn.close()
        logMsg = "*** Connection closed ***"
        print(logMsg)
        self.logger.logInfo(logMsg)

if __name__ == "__main__":
    deck = deck()
    deck.createDeck()
    print(deck.cards)