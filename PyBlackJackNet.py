#!/usr/bin/env python3
# Definiert die Klassen für das PyBlackJack-Netzwerkspiel
# In der nächsten Version sollen anstelle der Namen Bitmap-Dateien, die Bitmaps übertragen werden
# so dass sie auf dem Client angezeigt werden können

# TODOS: 
# card-Klasse ohne ImgPath, nur Name, Suit und Value
# Einsatz bei Player - wird beim Aufruf von Start() übergeben
# Neues Spiel legt immer neues Player und Dealer-Objekt an
# Noch einmal prüfen, ob OOP konsequent umgesetzt wurde

from datetime import datetime
from os import path, listdir
import random
import socket
import json
from json import JSONEncoder

from enum import Enum

class Suit(Enum):
    Heart = 0
    Diamond = 1
    Spade = 2
    Club = 3

dicCards = {"One":1,"Two":2,"Three":3,"Four":4,"Five":5,"Six":6,
            "Seven":7,"Eight":8,"Nine":9,"Ten":10,
            "Jack":10,"Queen":10,"King":10,"Ace":10}

# Ableiten von str, damit Umwandlung in JSON später einfacher wird
class gameState(str, Enum):
    OK = "0"
    DelaerBlackJack = "1"
    PlayerBlackJack = "2"
    DealerWin = "3"
    PlayerWin = "4"
    DealerLoose= "5"
    PlayerLoose = "6"
    Push = "7"

class gameSession:

    def __init__(self):
        self.dealerHand = []
        self.playerHand = []
        self.gameStart = datetime.today()


class Message(object):

    def __init__(self, Id, Header, Body):
        self.id = Id
        self.header = Header
        self.body = Body
        self.status = 0

class MsgEncoder(JSONEncoder):

    def default(self, o):
        return o.__dict__

class DatetimeEncoder(JSONEncoder):

    def default(self, obj):
        # if isinstance(obj, (datetime.date, datetime.datetime)):
        if isinstance(obj, datetime):
            return obj.isoformat()

# Definiert den (obligatorischen) Logger
class Logger:

    def __init__(self):
        self.logPath = path.join(path.dirname(__file__), "pyblackjackNet.log")
    
    def logInfo(self, msgText):
        with open(self.logPath, mode="a", encoding="utf8") as fh:
            timeStamp = datetime.today().strftime("%H:%M")
            fh.write(f"{timeStamp}: *** {msgText}\n")

    def logError(self, msgText):
        with open(self.logPath, mode="a", encoding="utf8") as fh:
            timeStamp = datetime.today().strftime("%H:%M")
            fh.write(f"{timeStamp}: !!! {msgText}\n")

# Definiert eine Karte
class Card:

    def __init__(self, Index, Name, Suit, Value):
        self.index = Index
        self.name = Name
        self.suit = Suit
        self.value = Value

    def __repr__(self):
        return f"{self.name} of {self.suit} ({self.value})"

class ClassEncoder(JSONEncoder):

    def default(self, o):
        return o.__dict__

# Definiert ein Kartendeck mit 52 Karten
class Deck:

    def createDeck(self):
        cards = []
        i = 0
        for col in Suit.__dict__.keys():
            if not col.startswith("_"):
                for c in dicCards.items():
                    i += 1
                    cards.append(Card(i, c[0] + "_" + col, col, c[1]))
        return cards

    def __init__(self):
        self.cards = self.createDeck()

# Definiert den Dealer des Spiels
class Dealer:

    def __init__(self, Game, Deck, Name):
        self.game = Game
        self.id = 1
        self.name = Name
        self.deck = Deck
        self.cardHand = []
        self.logger = Game.logger

    def getValue(self):
        value = self.game.Calculate(self.cardHand)
        return value

    def newCard(self, DealerCard=True):
        exitFlag = False
        while not exitFlag:
            z = random.randint(0, len(self.deck.cards) - 1)
            if z not in [c.index for c in self.cardHand]:
                newCard = self.deck.cards[z]
                # Ist die Karte für den Dealer?
                if DealerCard:
                    self.cardHand.append(newCard)
                exitFlag = True
        name = "Dealer" if DealerCard else "Player"
        logMsg = f"Calling newCard() for {name} with card={newCard}"
        self.logger.logInfo(logMsg)
        return newCard

# Definiert einen Spieler
class Player:

    def __init__(self, Game, Dealer, Name, Einsatz):
        self.game = Game
        self.dealer = Dealer
        self.id = 2
        self.name = Name
        self.einsatz = Einsatz
        self.cardHand = []
        self.logger = Game.logger
    
    # Dealer gibt eine Karte
    def hit(self):
        newCard = self.dealer.newCard(False)
        self.cardHand.append(newCard)
        logMsg = "Calling hit() for Player"
        self.logger.logInfo(logMsg)
        return newCard

    def getValue(self):
        value = self.game.Calculate(self.cardHand)
        return value

# Definiert ein Spiel
class Game:

    # Statische Variablen
    cardsPath = path.join(path.dirname(__file__), "Cards")
    defaultCardname = "green_back.png"
    defaultCardpath = path.join(cardsPath, defaultCardname)

    HOST = "127.0.0.1"
    PORT = 64000
    BUFSIZE = 2048

   # Berechnet den Wert eines Kartenstapels
    def Calculate(self, cards):
        value = 0
        for card in cards:
            value += card.value
        # Muessen die Asse anders gezaehlt werden?
        if value > 21:
            asse = [c for c in cards if c.name == "Ace"]
            for _ in asse:
                # Pro Ass 10 Punkte abziehen, aus Ass+Koenig+9 = 30 wird so 20
                value -= 10
        return value
    
    def Connect(self):
        global conn
        msgText = "*** Der PyBlackJack Services wartet auf eine Verbindung ***"
        print(msgText)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sh:
            sh.bind((Game.HOST, Game.PORT))
            sh.listen()
            conn, addr = sh.accept()
            msgText = "*** Verbindung wurde hergestellt ***"
            print(msgText)
            abbruch = False
            with conn:
                msgText = "Connected"
                self.logger.logInfo(msgText)
                while not abbruch:
                    data = conn.recv(256)
                    # Bytes in Text konvertieren und alle Leerzeichen abschneiden
                    cmd = data.decode("utf8").strip()
                    print(f"+++ -> {cmd}")
                    # Abbrechen, wenn keine Daten
                    if not cmd:
                        abbruch = True
                    # Parameter extrahieren, wenn vorhanden, z.B. start:100
                    cmdPara = 0
                    if ":" in cmd:
                        cmd, cmdPara = cmd.split(":")
                    # Alle Kommandos abfragen
                    if cmd == "start":
                        msgText = "start-command"
                        self.logger.logInfo(msgText)
                        # Spiel starten
                        # GameId = 1000 (nur provisorisch)
                        einsatz = int(cmdPara)
                        self.Start(1000, einsatz)
                        header = f" Game wurde mit GameId={self.gameId} und einem Einsatz von {einsatz} gestartet"
                        body = None
                        msg = Message(self.gameId, header, body)
                        jsonText = json.dumps(msg, indent=4, cls=MsgEncoder)
                        conn.sendall(bytes(jsonText,encoding="utf8"))
                    elif cmd == "deal":
                        # Beim ersten Deal können weder Dealer noch Player über 21 kommen, da das zweite Ass mit 1 gezählt wird
                        # Die einzige Abbruch-Situation ist daher ein BlackJack, also Bild + Ass
                        msgText = "deal-command"
                        self.logger.logInfo(msgText)
                        # Start einer neuen GameSession
                        # !!! Hier fehlt noch etwas !!! - die Frage ist, ob die Karten beim Dealer-/Player verwaltet werden oder
                        # als Teil GameSession
                        self.currentGame = gameSession()

                        header = "Dealer gibt sich und dem Player zwei Karten"
                        newCard = self.currentDealer.newCard(True)
                        print(f"+++ -> Dealer gibt sich eine Karte: {newCard}")
                        self.dealerCards.append(newCard)
                        newCard = self.currentDealer.newCard(True)
                        print(f"+++ -> Dealer gibt sich eine Karte: {newCard}")
                        self.dealerCards.append(newCard)
                        # Hat der Dealer einen Black Jack?
                        dealerValue = self.Calculate(self.dealerCards)
                        print(f"+++ -> Aktueller Dealer-Value: {dealerValue}")
                        if dealerValue == 21:
                            spielStatus = gameState.DelaerBlackJack
                            print(f"+++ -> Der Dealer hat einen BlackJack")
                        else:
                            # Player erhält zwei Karten
                            newCard = self.currentDealer.newCard()
                            print(f"+++ -> Dealer gibt Player eine Karte: {newCard}")
                            self.playerCards.append(newCard)
                            newCard = self.currentDealer.newCard(True)
                            print(f"+++ -> Dealer gibt Player eine Karte: {newCard}")
                            self.playerCards.append(newCard)
                            # Hat der Player einen Black Jack?
                            playerValue = self.Calculate(self.playerCards)
                            print(f"+++ -> Aktueller Player-Value: {playerValue}")
                            if playerValue == 21:
                                spielStatus = gameState.PlayerBlackJack
                                print(f"+++ -> Der Player hat einen BlackJack")
                            else:
                                spielStatus = gameState.OK
                        # body = [self.dealerCards, self.playerCards]
                        body = [self.dealerCards, self.playerCards, dealerValue, playerValue]
                        msg = Message(self.gameId, header, body)
                        msg.status = spielStatus
                        jsonText = json.dumps(msg, indent=4, cls=MsgEncoder, default = lambda x: x.__dict__)
                        conn.sendall(bytes(jsonText,encoding="utf8"))
                    elif cmd == "hit":
                        msgText = "hit-command"
                        self.logger.logInfo(msgText)
                        header = "Dealer gibt Player eine weitere Karte"
                        newCard = self.currentDealer.newCard()
                        print(f"+++ -> Dealer gibt Player eine Karte: {newCard}")
                        self.playerCards.append(newCard)
                        # Hat der Player gewonnen oder verloren?
                        playerValue = self.Calculate(self.playerCards)
                        print(f"+++ -> Aktueller Player-Value: {playerValue}")
                        if playerValue == 21:
                            spielStatus = gameState.PlayerWin
                            print(f"+++ -> Der Player hat einen BlackJack")
                        elif playerValue > 21:
                            spielStatus = gameState.PlayerLoose
                            print(f"+++ -> Der Player hat verloren")
                        else:
                            spielStatus = gameState.OK
                        # body = [self.dealerCards, self.playerCards]
                        body = [self.dealerCards, self.playerCards, dealerValue, playerValue]
                        msg = Message(self.gameId, header, body)
                        msg.status = spielStatus
                        jsonText = json.dumps(msg, indent=4, cls=MsgEncoder, default = lambda x: x.__dict__)
                        conn.sendall(bytes(jsonText,encoding="utf8"))
                    elif cmd == "stand":
                        msgText = "stand-command"
                        self.logger.logInfo(msgText)
                        header = "Dealer gibt sich weitere Karten bis das Spiel zu Ende ist"
                        abbruch = False
                        while not abbruch:
                            newCard = self.currentDealer.newCard()
                            print(f"+++ -> Dealer gibt sich eine Karte: {newCard}")
                            self.dealerCards.append(newCard)
                            # Hat der Dealer gewonnen oder verloren?
                            dealerValue = self.Calculate(self.dealerCards)
                            print(f"+++ -> Aktueller Dealer-Value: {dealerValue}")
                            playerValue = self.Calculate(self.playerCards)
                            print(f"+++ -> Aktueller Player-Value: {playerValue}")
                            if dealerValue > 21:
                                spielStatus = gameState.DealerLoose
                                print(f"+++ -> Der Dealer hat verloren")
                            elif dealerValue > playerValue:
                                spielStatus = gameState.DealerWin
                                print(f"+++ -> Der Dealer hat gewonnen")
                            elif dealerValue == 21:
                                spielStatus = gameState.DealerWin
                                print(f"+++ -> Der Dealer hat einen BlackJack")
                            elif dealerValue == playerValue:
                                spielStatus = gameState.Push
                            else:
                                spielStatus = gameState.OK
                            if spielStatus != gameState.OK:
                                abbruch = True
                        msg = Message(self.gameId, header, body)
                        msg.status = spielStatus
                        # body = [self.dealerCards, self.playerCards]
                        body = [self.dealerCards, self.playerCards, dealerValue, playerValue    ]
                        jsonText = json.dumps(msg, indent=4, cls=MsgEncoder, default = lambda x: x.__dict__)
                        conn.sendall(bytes(jsonText,encoding="utf8"))
                    elif cmd == "quit":
                        msgText = "quit-command"
                        self.logger.logInfo(msgText)
                        header = "Game terminated"
                        body = None
                        msg = Message(self.gameId, header, body)
                        jsonText = json.dumps(msg, indent=4, cls=MsgEncoder)
                        conn.sendall(bytes(jsonText,encoding="utf8"))
                        abbruch = True
                    else:
                        msgText = "unknown command"
                        self.logger.logInfo(msgText)

    def Disconnect(self):
        global conn
        conn.close()
        msgText = "*** Verbindung wurde geschlossen ***"
        print(msgText)

    def __init__(self, Logger):
        self.logger = Logger

    def Start(self, Id, Einsatz):
        self.gameId = Id
        self.startTime = datetime.today()
        # Alle Listen initialisieren
        self.dealerCards = []
        self.playerCards = []
        msgText = f"Game started at {self.startTime:%H:%M}"
        self.logger.logInfo(msgText)
        self.currentDeck = Deck()
        self.currentDealer = Dealer(self, self.currentDeck, "The Dealer")
        self.currentPlayer = Player(self, self.currentDealer, "Petro", Einsatz)

if __name__ == "__main__":
    deck = Deck()
    deck.createDeck()
    print(deck.cards)